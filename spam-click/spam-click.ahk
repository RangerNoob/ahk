﻿#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
; #Warn  ; Enable warnings to assist with detecting common errors.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.
#SingleInstance force

; ----------------------------------------------------------------
; toggle on off right / left mouse button spam with this script
RIGHT_SPAM := False
LEFT_SPAM  := True
ClickDelay() {
    Random, randDelay, 65, 105
    return randDelay
}
; ----------------------------------------------------------------

; ^!r::Reload  ; Assign Ctrl-Alt-R as a hotkey to restart the script.
Menu, Tray, Icon, arrow_small.ico

ShouldSpam := False
f12::
    if ShouldSpam
	{
		ShouldSpam := False
        Menu, Tray, Icon, arrow_small.ico
        SetCursorToInverted()
	}
	else
	{
		ShouldSpam := True
        Menu, Tray, Icon, arrow_large.ico
        SetCursorToLarge()
	}
	return

; spam left mouse button
~$LButton::
    if ShouldSpam & LEFT_SPAM
    {
        While GetKeyState("LButton", "P"){
            Sleep ClickDelay()
            Click
        }
    }
    Return

; spam right mouse button
~$RButton::
    if ShouldSpam & RIGHT_SPAM
    {
        While GetKeyState("RButton", "P"){
            Sleep ClickDelay()
            Click, right
        }
    }
    Return



SetCursorToInverted() {
    ; https://autohotkey.com/docs/commands/RegWrite.htm
    RegWrite, REG_EXPAND_SZ, HKEY_CURRENT_USER\Control Panel\Cursors, AppStarting, `%SystemRoot`%\cursors\wait_i.cur
    RegWrite, REG_EXPAND_SZ, HKEY_CURRENT_USER\Control Panel\Cursors, Arrow, `%SystemRoot`%\cursors\arrow_i.cur
    RegWrite, REG_EXPAND_SZ, HKEY_CURRENT_USER\Control Panel\Cursors, Crosshair, `%SystemRoot`%\cursors\cross_i.cur
    RegWrite, REG_EXPAND_SZ, HKEY_CURRENT_USER\Control Panel\Cursors, Hand, `%SystemRoot`%\cursors\aero_link_i.cur
    RegWrite, REG_EXPAND_SZ, HKEY_CURRENT_USER\Control Panel\Cursors, Help, `%SystemRoot`%\cursors\help_i.cur
    RegWrite, REG_EXPAND_SZ, HKEY_CURRENT_USER\Control Panel\Cursors, IBeam, `%SystemRoot`%\cursors\beam_i.cur
    RegWrite, REG_EXPAND_SZ, HKEY_CURRENT_USER\Control Panel\Cursors, No, `%SystemRoot`%\cursors\no_i.cur
    RegWrite, REG_EXPAND_SZ, HKEY_CURRENT_USER\Control Panel\Cursors, NWPen, `%SystemRoot`%\cursors\pen_i.cur
    RegWrite, REG_EXPAND_SZ, HKEY_CURRENT_USER\Control Panel\Cursors, Person, `%SystemRoot`%\cursors\person_i.cur
    RegWrite, REG_EXPAND_SZ, HKEY_CURRENT_USER\Control Panel\Cursors, Pin, `%SystemRoot`%\cursors\pin_i.cur
    RegWrite, REG_EXPAND_SZ, HKEY_CURRENT_USER\Control Panel\Cursors, SizeAll, `%SystemRoot`%\cursors\move_i.cur
    RegWrite, REG_EXPAND_SZ, HKEY_CURRENT_USER\Control Panel\Cursors, SizeNESW, `%SystemRoot`%\cursors\size1_i.cur
    RegWrite, REG_EXPAND_SZ, HKEY_CURRENT_USER\Control Panel\Cursors, SizeNS, `%SystemRoot`%\cursors\size4_i.cur
    RegWrite, REG_EXPAND_SZ, HKEY_CURRENT_USER\Control Panel\Cursors, SizeNWSE, `%SystemRoot`%\cursors\size2_i.cur
    RegWrite, REG_EXPAND_SZ, HKEY_CURRENT_USER\Control Panel\Cursors, SizeWE, `%SystemRoot`%\cursors\size3_i.cur
    RegWrite, REG_EXPAND_SZ, HKEY_CURRENT_USER\Control Panel\Cursors, UpArrow, `%SystemRoot`%\cursors\up_i.cur
    RegWrite, REG_EXPAND_SZ, HKEY_CURRENT_USER\Control Panel\Cursors, Wait, `%SystemRoot`%\cursors\busy_i.cur

    ; https://stackoverflow.com/questions/41713827/programmatically-change-custom-mouse-cursor-in-windows
    SPI_SETCURSORS := 0x57
    SPIF_UPDATEINIFILE = 0x01
    SPIF_SENDCHANGE = 0x02
    result := DllCall("SystemParametersInfo", "UInt", SPI_SETCURSORS, "UInt", 0, "UInt", null, "UInt", SPIF_UPDATEINIFILE | SPIF_SENDCHANGE)
    ; MsgBox Error Level: %ErrorLevel% `nLast error: %A_LastError%`nresult: %result%
}

SetCursorToLarge() {
    ; https://autohotkey.com/docs/commands/RegWrite.htm
    RegWrite, REG_EXPAND_SZ, HKEY_CURRENT_USER\Control Panel\Cursors, AppStarting, `%SystemRoot`%\cursors\wait_l.cur
    RegWrite, REG_EXPAND_SZ, HKEY_CURRENT_USER\Control Panel\Cursors, Arrow, `%SystemRoot`%\cursors\arrow_l.cur
    RegWrite, REG_EXPAND_SZ, HKEY_CURRENT_USER\Control Panel\Cursors, Crosshair, `%SystemRoot`%\cursors\cross_l.cur
    RegWrite, REG_EXPAND_SZ, HKEY_CURRENT_USER\Control Panel\Cursors, Hand, `%SystemRoot`%\cursors\aero_link_l.cur
    RegWrite, REG_EXPAND_SZ, HKEY_CURRENT_USER\Control Panel\Cursors, Help, `%SystemRoot`%\cursors\help_l.cur
    RegWrite, REG_EXPAND_SZ, HKEY_CURRENT_USER\Control Panel\Cursors, IBeam, `%SystemRoot`%\cursors\beam_l.cur
    RegWrite, REG_EXPAND_SZ, HKEY_CURRENT_USER\Control Panel\Cursors, No, `%SystemRoot`%\cursors\no_l.cur
    RegWrite, REG_EXPAND_SZ, HKEY_CURRENT_USER\Control Panel\Cursors, NWPen, `%SystemRoot`%\cursors\pen_l.cur
    RegWrite, REG_EXPAND_SZ, HKEY_CURRENT_USER\Control Panel\Cursors, Person, `%SystemRoot`%\cursors\person_l.cur
    RegWrite, REG_EXPAND_SZ, HKEY_CURRENT_USER\Control Panel\Cursors, Pin, `%SystemRoot`%\cursors\pin_l.cur
    RegWrite, REG_EXPAND_SZ, HKEY_CURRENT_USER\Control Panel\Cursors, SizeAll, `%SystemRoot`%\cursors\move_l.cur
    RegWrite, REG_EXPAND_SZ, HKEY_CURRENT_USER\Control Panel\Cursors, SizeNESW, `%SystemRoot`%\cursors\size1_l.cur
    RegWrite, REG_EXPAND_SZ, HKEY_CURRENT_USER\Control Panel\Cursors, SizeNS, `%SystemRoot`%\cursors\size4_l.cur
    RegWrite, REG_EXPAND_SZ, HKEY_CURRENT_USER\Control Panel\Cursors, SizeNWSE, `%SystemRoot`%\cursors\size2_l.cur
    RegWrite, REG_EXPAND_SZ, HKEY_CURRENT_USER\Control Panel\Cursors, SizeWE, `%SystemRoot`%\cursors\size3_l.cur
    RegWrite, REG_EXPAND_SZ, HKEY_CURRENT_USER\Control Panel\Cursors, UpArrow, `%SystemRoot`%\cursors\up_l.cur
    RegWrite, REG_EXPAND_SZ, HKEY_CURRENT_USER\Control Panel\Cursors, Wait, `%SystemRoot`%\cursors\busy_l.cur

    ; https://stackoverflow.com/questions/41713827/programmatically-change-custom-mouse-cursor-in-windows
    SPI_SETCURSORS := 0x57
    SPIF_UPDATEINIFILE = 0x01
    SPIF_SENDCHANGE = 0x02
    result := DllCall("SystemParametersInfo", "UInt", SPI_SETCURSORS, "UInt", 0, "UInt", null, "UInt", SPIF_UPDATEINIFILE | SPIF_SENDCHANGE)
    ; MsgBox Error Level: %ErrorLevel% `nLast error: %A_LastError%`nresult: %result%
}